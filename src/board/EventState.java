package board;

/**
 * this enum is used to determine what kind of game event we are dealing with.
 * */

public enum EventState {
    INITIAL_PLACING, ATTACK, MOVE, NOTHING, WAIT
}
