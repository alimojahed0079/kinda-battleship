package board;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import troops.TroopsBluePrint;

import java.util.ArrayList;
import java.util.Arrays;


public class PlayerBoard extends GridPane {


    /**
     * Ther player board class is used to store the cells and the their information and interact with player when it needed.
     * */

    private ArrayList<TroopsBluePrint> troops = new ArrayList<>();
    private Cell[][] grid;
    //private CellState[][] gridControl;
    private String playerName;
    private TroopsBluePrint currentTrooper;
    private EventState state;
    private static int cellSize = 20;
    private static int numberOfCells = 20;
    private ArrayList<Integer> lastXCoords;
    private ArrayList<Integer> lastYCoords;


    PlayerBoard(String playerName) {
        grid = new Cell[numberOfCells][numberOfCells];
        currentTrooper = null;
        for (int i = 0; i < numberOfCells; i++) {
            for (int j = 0; j < numberOfCells; j++) {
                Cell temp = new Cell(this, i, j);
                grid[i][j] = temp;
                add(temp, i, j);
            }
        }
        lastXCoords = new ArrayList<>();
        lastYCoords = new ArrayList<>();
        this.playerName = playerName;
    }



    /**
     * This method is used to delete information about a trooper need to be moved.
     * */

    public void makeGridReadyForMove()
    {
        if (currentTrooper != null) {
            System.out.println(Arrays.toString(currentTrooper.getxCoordsOnGrid().toArray()));
            System.out.println(Arrays.toString(currentTrooper.getyCoordsOnGrid().toArray()));
            for (int i = 0; i < currentTrooper.getxCoordsOnGrid().size(); i++) {
                int x = currentTrooper.getxCoordsOnGrid().get(i);
                int y = currentTrooper.getyCoordsOnGrid().get(i);
                grid[x][y].setCellState(CellState.EMPTY);
                grid[x][y].setTrooper(null);
                grid[x][y].setHasShip(false);
                grid[x][y].setAvailable(true);

                System.out.println(x + ", " + y);
            }
            troops.remove(currentTrooper);
            currentTrooper.getxCoordsOnGrid().clear();
            currentTrooper.getyCoordsOnGrid().clear();

        }
    }
    public int getNumberOfCurrentTroops()
    {
        int counter = 0;
        for (TroopsBluePrint trooper: troops)
        {
            if (!trooper.isDestroy())
            {
                counter++;
            }
        }
        return counter;
    }


    /**
     * This method is used to paint the cells and show the place of troops. this method is used whrn we need to move our troops.
     * */
    public void paintingForPlayer()
    {
        for (int i = 0; i < numberOfCells; i++) {
            for (int j = 0; j < numberOfCells; j++) {
                if (grid[i][j].getCellState() == CellState.HIT) {
                    grid[i][j].getBorderRect().setFill(Color.YELLOW);
                } else if (grid[i][j].getCellState() == CellState.DESTROY) {
                    grid[i][j].getBorderRect().setFill(Color.RED);
                } else if(grid[i][j].getCellState() == CellState.EMPTY){
                    grid[i][j].getBorderRect().setFill(Color.BLACK);
                }
                else
                {
                    grid[i][j].getBorderRect().setFill(Color.BLUE);
                }
            }
        }
    }


    /**
     *
     * This method is used to paint the cells but it won't show the place of troops.
     * */
    public void repainting() {

        for (int i = 0; i < numberOfCells; i++) {
            for (int j = 0; j < numberOfCells; j++) {
                if (grid[i][j].getCellState() == CellState.HIT) {
                    grid[i][j].getBorderRect().setFill(Color.YELLOW);
                } else if (grid[i][j].getCellState() == CellState.DESTROY) {
                    if (grid[i][j].getTrooper()!= null) {
                        if (grid[i][j].getTrooper().isDestroy()) {
                            grid[i][j].getBorderRect().setFill(Color.RED);
                            grid[i][j].setCellState(CellState.DESTROY);
                        }
                    }
                } else {
                    grid[i][j].getBorderRect().setFill(Color.BLACK);
                }
                if (grid[i][j].getTrooper()!= null) {
                    if (grid[i][j].getTrooper().isDestroy()) {
                        grid[i][j].getBorderRect().setFill(Color.RED);
                        grid[i][j].setCellState(CellState.DESTROY);
                    }
                }

            }

        }



        for (int i = 0; i < lastXCoords.size(); i++) {
            int x = lastXCoords.get(i);
            int y = lastYCoords.get(i);
            grid[x][y].getBorderRect().setFill(Color.PINK);
        }
    }
    public boolean isAllDestroy()
    {
        for (TroopsBluePrint trooper: troops)
        {
            if (!trooper.isDestroy())
                return false;
        }
        return true;
    }
    public Cell[][] getGrid() {
        return grid;
    }

    public Cell getCell(int x, int y) {
        return grid[x][y];
    }


    public ArrayList<TroopsBluePrint> getTroops() {
        return troops;
    }

    public void setTroops(ArrayList<TroopsBluePrint> troops) {
        this.troops = troops;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public TroopsBluePrint getCurrentTrooper() {
        return currentTrooper;
    }

    public void setCurrentTrooper(TroopsBluePrint currentTrooper) {
        this.currentTrooper = currentTrooper;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public static int getCellSize() {
        return cellSize;
    }

    public static void setCellSize(int cellSize) {
        PlayerBoard.cellSize = cellSize;
    }

    public static int getNumberOfCells() {
        return numberOfCells;
    }

    public static void setNumberOfCells(int numberOfCells) {
        PlayerBoard.numberOfCells = numberOfCells;
        if (numberOfCells == 10)
        {
            cellSize = 40;
        }
        if (numberOfCells>10 && numberOfCells<=15)
        {
            cellSize = 30;
        }
        if(numberOfCells>15 && numberOfCells <= 20)
        {
            cellSize = 20;
        }
        if (numberOfCells >20)
        {
            cellSize = 10;
        }
    }

    public void setGrid(Cell[][] grid) {
        this.grid = grid;
    }

    public ArrayList<Integer> getLastXCoords() {
        return lastXCoords;
    }

    public void setLastXCoords(ArrayList<Integer> lastXCoords) {
        this.lastXCoords = lastXCoords;
    }

    public ArrayList<Integer> getLastYCoords() {
        return lastYCoords;
    }

    public void setLastYCoords(ArrayList<Integer> lastYCoords) {
        this.lastYCoords = lastYCoords;
    }
}
