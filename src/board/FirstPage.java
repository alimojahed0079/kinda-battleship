package board;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;


import java.net.URL;
import java.util.ResourceBundle;


/**
 * This class is a controller for FirstPage fxml file
 * */


public class FirstPage implements Initializable {
    @FXML
    public Button playGameButton;
    @FXML
    public Button quit;
    @FXML
    public Label title;
    @FXML
    public ImageView image;
    @FXML
    public Label myName;
    @FXML
    public Label courseTitle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    public Button getPlayGameButton() {
        return playGameButton;
    }

    public Button getQuit() {
        return quit;
    }


}
