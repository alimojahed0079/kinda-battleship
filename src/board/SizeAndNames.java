package board;

import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class SizeAndNames implements Initializable {

    /**
     * This class is a controller for SizeAndNames.fxml file.
     * */

    @FXML
    public Button initialing;
    @FXML
    public Button back;
    @FXML
    public TextField numberOfCellsField;
    @FXML
    public TextField player1Name;
    @FXML
    public TextField player2Name;
    @FXML
    public Label numberOfCellsLabel;
    @FXML
    public Label player1NameLabel;
    @FXML
    public Label player2NameLabel;
    @FXML
    public Slider numberOfCellsSlider;




    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BooleanBinding booleanBinding = new BooleanBinding() {
            {
                super.bind(player1Name.textProperty(), player2Name.textProperty());
            }
            @Override
            protected boolean computeValue() {
                return (player1Name.getText().isEmpty() || player2Name.getText().isEmpty());
            }

        };
        initialing.disableProperty().bind(booleanBinding);
        numberOfCellsSlider.setMin(10);
        numberOfCellsSlider.setMax(40);
        numberOfCellsSlider.setMajorTickUnit(5);
        numberOfCellsSlider.setMinorTickCount(1);
        numberOfCellsSlider.setShowTickMarks(true);
        numberOfCellsSlider.setShowTickLabels(true);
        numberOfCellsSlider.setSnapToTicks(true);

    }

    public Button getInitialing() {
        return initialing;
    }

    public void setInitialing(Button initialing) {
        this.initialing = initialing;
    }

    public Button getBack() {
        return back;
    }

    public void setBack(Button back) {
        this.back = back;
    }

    public TextField getNumberOfCellsField() {
        return numberOfCellsField;
    }

    public void setNumberOfCellsField(TextField numberOfCellsField) {
        this.numberOfCellsField = numberOfCellsField;
    }

    public TextField getPlayer1Name() {
        return player1Name;
    }

    public void setPlayer1Name(TextField player1Name) {
        this.player1Name = player1Name;
    }

    public TextField getPlayer2Name() {
        return player2Name;
    }

    public void setPlayer2Name(TextField player2Name) {
        this.player2Name = player2Name;
    }

    public Label getNumberOfCellsLabel() {
        return numberOfCellsLabel;
    }

    public void setNumberOfCellsLabel(Label numberOfCellsLabel) {
        this.numberOfCellsLabel = numberOfCellsLabel;
    }

    public Label getPlayer1NameLabel() {
        return player1NameLabel;
    }

    public void setPlayer1NameLabel(Label player1NameLabel) {
        this.player1NameLabel = player1NameLabel;
    }

    public Label getPlayer2NameLabel() {
        return player2NameLabel;
    }

    public void setPlayer2NameLabel(Label player2NameLabel) {
        this.player2NameLabel = player2NameLabel;
    }

    public Slider getNumberOfCellsSlider() {
        return numberOfCellsSlider;
    }

    public void setNumberOfCellsSlider(Slider numberOfCellsSlider) {
        this.numberOfCellsSlider = numberOfCellsSlider;
    }
}
