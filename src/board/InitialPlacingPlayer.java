package board;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import temporary_classes.AlertBox;
import troops.*;

import java.net.URL;
import java.util.ResourceBundle;


public class InitialPlacingPlayer implements Initializable {


    /**
     *
     * This class is used as a controller for initialPlacingPlayer.fxml file.
     * We use this file to place the troops on the table
     * */

    private PlayerBoard[] playerBoards;
    @FXML
    private AnchorPane gridContainer;
    @FXML
            private GridPane buttonContainer;
    @FXML
            private Button setButton;
    @FXML
    private Label details;

    public static int currentPlayer;
    private Soldier[][] soldiers;
    private Knight[][] knights;
    private Tower[][] towers;
    private Commander[] commander;
    private SimpleBooleanProperty setButtonVisibility = new SimpleBooleanProperty();
    private static String player1Name;
    private static String player2Name;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        playerBoards = new PlayerBoard[2];
        currentPlayer = 0;
        soldiers = new Soldier[2][5];
        knights = new Knight[2][3];
        towers = new Tower[2][2];
        commander = new Commander[2];
        setButton.setDisable(true);
        reset();
    }

    public Button getSetButton() {
        return setButton;
    }

    /**
     * The reset method is just set things up for gui show and set the information from last scene to this one and set up listeners
     * for the components we need to interact with.
     * */

    public void reset()
    {

        playerBoards[0] = new PlayerBoard(player1Name);
        playerBoards[1] = new PlayerBoard(player2Name);
        AlertBox.display("A friendly reminder", "Make sure " + playerBoards[1-currentPlayer].getPlayerName() + " doesn't watch your board 😉");
        details.setText(playerBoards[currentPlayer].getPlayerName()+" have to complete your border");
        buttonContainer.setPadding(new Insets(2, 2, 2, 2));

        playerBoards[currentPlayer].setState(EventState.INITIAL_PLACING);
        soldiers[currentPlayer] = new Soldier[5];
        knights[currentPlayer] = new Knight[3];
        towers[currentPlayer] = new Tower[2];
        commander[currentPlayer] = new Commander();

        for (int i=0; i< soldiers[currentPlayer].length; i++)
        {
            soldiers[currentPlayer][i] = new Soldier(i+1);
            troopsButtonListener(soldiers[currentPlayer][i]);

        }
        buttonContainer.add(soldiers[currentPlayer][0].getButton(), 0 , 0);
        buttonContainer.add(soldiers[currentPlayer][1].getButton(), 1, 0);
        buttonContainer.add(soldiers[currentPlayer][2].getButton(), 0, 1);
        buttonContainer.add(soldiers[currentPlayer][3].getButton(), 1, 1);
        buttonContainer.add(soldiers[currentPlayer][4].getButton(),0 , 2);
        for (int i=0; i<knights[currentPlayer].length;i++)
        {
            knights[currentPlayer][i] = new Knight(i+1);
            troopsButtonListener(knights[currentPlayer][i]);

        }
        buttonContainer.add(knights[currentPlayer][0].getButton(), 0, 3);
        buttonContainer.add(knights[currentPlayer][1].getButton(), 1, 3);
        buttonContainer.add(knights[currentPlayer][2].getButton(), 0, 4);

        towers[currentPlayer][0] = new Tower(1);
        troopsButtonListener(towers[currentPlayer][0]);
        towers[currentPlayer][1] = new Tower(2);
        troopsButtonListener(towers[currentPlayer][1]);

        buttonContainer.add(towers[currentPlayer][0].getButton(),0,5);
        buttonContainer.add(towers[currentPlayer][1].getButton(), 1, 5);

        troopsButtonListener(commander[currentPlayer]);
        buttonContainer.add(commander[currentPlayer].getButton(), 0, 6);

        //cellListener();
        gridContainer.getChildren().remove(playerBoards[currentPlayer]);
        gridContainer.getChildren().add(playerBoards[currentPlayer]);
        cellListener();

    }


    public static void setPlayer1Name(String player1Name) {
        InitialPlacingPlayer.player1Name = player1Name;
    }


    public static void setPlayer2Name(String player2Name) {
        InitialPlacingPlayer.player2Name = player2Name;
    }

    public GridPane getButtonContainer() {
        return buttonContainer;
    }



    /**
     * These methods are used for setting up listeners for the components we need to interact with.
     * */
    private void troopsButtonListener(TroopsBluePrint trooper)
    {
        Button temp = trooper.getButton();

        temp.setOnAction(event -> {
            playerBoards[currentPlayer].setState(EventState.INITIAL_PLACING);
            playerBoards[currentPlayer].setCurrentTrooper(trooper);
            if (trooper.isSet())
            {
                playerBoards[currentPlayer].makeGridReadyForMove();
                trooper.setSet(false);
            }
            setButtonVisibility.set(playerBoards[currentPlayer].getTroops().size() < 11);
            setButton.disableProperty().bind(setButtonVisibility);
            playerBoards[currentPlayer].paintingForPlayer();
        });


    }

    private void cellListener()
    {
        Cell[][] grid = playerBoards[currentPlayer].getGrid();
        int id = 0;
        for(int i=0; i < PlayerBoard.getNumberOfCells(); i++)
        {
            for (int j = 0 ; j < PlayerBoard.getNumberOfCells(); j++)
            {
                try {
                    Cell temp = playerBoards[currentPlayer].getCell(i,j);
                    mouseClickedHandler(temp);
                    mouseEnteredHandler(temp);
                    mouseExitedHandler(temp);
                }
                catch (Exception e)
                {
                    System.out.println("not now" + id++);
                }

            }
        }
    }


    private void mouseExitedHandler(Cell cell)
    {
        cell.setOnMouseExited(event -> {

                playerBoards[currentPlayer].paintingForPlayer();
        });

    }

    private void mouseEnteredHandler(Cell cell)
    {
        cell.setOnMouseEntered(event -> {
            if (playerBoards[currentPlayer].getCurrentTrooper()!=null) {
                if (cell.isValidForPlacing()) {
                    cell.paintCells(Color.GREEN);
                }
            }
        });
    }

    private void mouseClickedHandler(Cell cell)
    {
        cell.setOnMouseClicked(event -> {
            if (playerBoards[currentPlayer].getCurrentTrooper() != null) {
                if (cell.isValidForPlacing()) {
                    cell.placeTrooper();
                    playerBoards[currentPlayer].paintingForPlayer();
                }
                playerBoards[currentPlayer].setCurrentTrooper(null);
                setButtonVisibility.set(playerBoards[currentPlayer].getTroops().size() < 11);
                setButton.disableProperty().bind(setButtonVisibility);
            }
        });
    }

    public PlayerBoard[] getPlayerBoards() {
        return playerBoards;
    }
}
