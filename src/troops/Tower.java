package troops;

public class Tower extends TroopsBluePrint{

    public Tower(int number) {
        super(4, 2, 2, 2, number);
        getButton().setText(this.toString());
    }

    @Override
    public int[] attack() {
        return new int[]{1, 3};
    }

    @Override
    public String toString() {
        return "Tower" + getNumber();
    }
}
