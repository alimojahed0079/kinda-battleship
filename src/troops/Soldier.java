package troops;

public class Soldier extends TroopsBluePrint{
    public Soldier(int number) {
        super(1, 1, 1, 0, number);
        getButton().setText(this.toString());
    }

    @Override
    public int[] attack() {
        return new int[]{1, 1};
    }

    @Override
    public String toString() {
        return "Soldier " + getNumber();
    }
}
