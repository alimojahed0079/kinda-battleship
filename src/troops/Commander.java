package troops;

public class Commander extends TroopsBluePrint {
    public Commander() {
        super(9, 3, 3, 3, 1);
        getButton().setText(this.toString());
    }

    @Override
    public int[] attack() {
        return new int[]{2, 2};
    }

    @Override
    public String toString() {
        return "Commander";
    }
}
