package troops;

import board.PlayerBoard;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;

import java.util.ArrayList;

public abstract class TroopsBluePrint extends StackPane {
    /**
     * This abstract class is used to determine common attributes and behaviours of all troops and
     * inherited by them.
     * */


    private boolean isSet;
    private boolean isReady = true;
    private int health;
    private int maxHealth;
    private int coolDownDuration;
    private int wait;
    private int xCoords;
    private int yCoords;
    private boolean canBeMoved;
    private int number;
    public boolean isCanBeMoved() {
        return canBeMoved;
    }

    public void setCanBeMoved(boolean canBeMoved) {
        this.canBeMoved = canBeMoved;
    }

    private ArrayList<Integer> xCoordsOnGrid;

    public ArrayList<Integer> getyCoordsOnGrid() {
        return yCoordsOnGrid;
    }

    public void setyCoordsOnGrid(ArrayList<Integer> yCoordsOnGrid) {
        this.yCoordsOnGrid = yCoordsOnGrid;
    }

    private ArrayList<Integer> yCoordsOnGrid;
    private Button button;


    private PlayerBoard board;
    public TroopsBluePrint(int health, int xCoords, int yCoords, int coolDownDuration, int number) {
        this.coolDownDuration = coolDownDuration;
        this.health = health;
        maxHealth = health;
        this.xCoords = xCoords;
        this.yCoords = yCoords;
        button = new Button();
        this.number = number;
        xCoordsOnGrid = new ArrayList<>();
        yCoordsOnGrid = new ArrayList<>();


    }

    public void setButton(Button button) {
        this.button = button;
    }

    public int getNumber() {
        return number;
    }


    public abstract int[] attack();

    public boolean isSet() {
        return isSet;
    }

    public void setSet(boolean set) {
        isSet = set;
    }

    public boolean isReady() {
        return isReady;
    }
    public void decreaseWait()
    {
        if (!isReady)
        {
            wait--;
        }
        if (wait == 0)
        {
            isReady = true;
        }
    }
    public void damage()
    {
        if (health > 0)
            this.health--;
    }

    public int getCoolDownDuration() {
        return coolDownDuration;
    }

    public boolean isDestroy()
    {
        return health == 0;
    }
    public void setWait(int wait) {
        this.wait = wait;
    }

    public void setCoolDownDuration(int coolDownDuration) {
        this.coolDownDuration = coolDownDuration;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getXCoords() {
        return xCoords;
    }

    public void setXCoords(int xCoords) {
        this.xCoords = xCoords;
    }

    public int getYCoords() {
        return yCoords;
    }

    public void setYCoords(int yCoords) {
        this.yCoords = yCoords;
    }

    public ArrayList<Integer> getxCoordsOnGrid() {
        return xCoordsOnGrid;
    }

    public void setxCoordsOnGrid(ArrayList<Integer> xCoordsOnGrid) {
        this.xCoordsOnGrid = xCoordsOnGrid;
    }

    public boolean isHit()
    {
        return health != maxHealth;
    }

    public void move(){
        if (!isHit())
        {
            for (Integer arr: xCoordsOnGrid)
            {
                arr = null;
            }
            isSet = false;
        }
    }

    public Button getButton() {
        return button;
    }

}
