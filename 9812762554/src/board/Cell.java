package board;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import troops.TroopsBluePrint;

public class Cell extends StackPane {
/**
 *
 * Cell Class is used to manage every space in player table, it's extends from StackPane
 * Class and thus it can have its own children and can be handled when an event occurs directly.
 *
 * */
    private PlayerBoard board;
    private CellState cellState = CellState.EMPTY;
    private TroopsBluePrint trooper;
    private int x, y;


    /**
     * This Rectangle is used to show this cell border.
     * */

    private Rectangle border = new Rectangle(PlayerBoard.getCellSize(), PlayerBoard.getCellSize());


    private boolean isAvailable = true;
    private boolean hasShip;
    private boolean canHit = true;

    Cell(PlayerBoard board, int x, int y) {
        this.board = board;
        this.x = x;
        this.y = y;
        border.setStroke(Color.LIGHTGRAY);
        this.getChildren().add(border);
    }



    public void setCanHit(boolean canHit) {
        this.canHit = canHit;
    }

    public boolean isCanHit() {
        return canHit;
    }


   public boolean isValidForPlacing() {
        if (board.getCurrentTrooper() != null) {
            for (int i = 0; i < board.getCurrentTrooper().getXCoords(); i++) {
                for (int j = 0; j < board.getCurrentTrooper().getYCoords(); j++) {
                    if (!isValidCoordinate(i, j)) {
                        return false;
                    }
                    Cell temp = board.getGrid()[this.x + i][this.y + j];
                    if (!temp.isAvailable() && temp.hasShip())
                        return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean isValidCoordinate(int x, int y) {
        return this.x + x < PlayerBoard.getNumberOfCells() && this.y + y < PlayerBoard.getNumberOfCells();
    }

    public void paintCells(Color color) {
        for (int i = 0; i < board.getCurrentTrooper().getXCoords(); i++) {
            for (int j = 0; j < board.getCurrentTrooper().getYCoords(); j++) {
                board.getGrid()[this.x + i][this.y + j].getBorderRect().setFill(color);
            }
        }
    }


    /**
     * This method is call when an Mouse Click Event occurs and thus a trooper is set to the table.
     *
     * */

    public void placeTrooper() {
        board.getTroops().add(board.getCurrentTrooper());
        board.getCurrentTrooper().setSet(true);
        for (int i = 0; i < board.getCurrentTrooper().getXCoords(); i++) {
            for (int j = 0; j < board.getCurrentTrooper().getYCoords(); j++) {
                board.getGrid()[this.x + i][this.y + j].setHasShip(true);
                board.getGrid()[this.x + i][this.y+j].setAvailable(false);
                board.getGrid()[this.x + i][this.y + j].setTrooper(board.getCurrentTrooper());
                board.getGrid()[this.x + i][this.y + j].setCellState(CellState.HAS_TROOPER);
                board.getGrid()[this.x + i][this.y + j].getBorderRect().setFill(Color.BLUE);
                board.getGrid()[this.x+i][this.y+j].getTrooper().getxCoordsOnGrid().add(this.x+i);
                board.getGrid()[this.x+i][this.y+j].getTrooper().getyCoordsOnGrid().add(this.y+j);
            }
        }
    }

    public PlayerBoard getBoard() {
        return board;
    }

    public void setBoard(PlayerBoard board) {
        this.board = board;
    }

    public CellState getCellState() {
        return cellState;
    }

    public void setCellState(CellState cellState) {
        this.cellState = cellState;
    }

    public TroopsBluePrint getTrooper() {
        return trooper;
    }

    public void setTrooper(TroopsBluePrint trooper) {
        this.trooper = trooper;
    }

    public int getX() {
        return x;
    }



    public int getY() {
        return y;
    }

    public Rectangle getBorderRect() {
        return border;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean hasShip() {
        return hasShip;
    }

    public void setHasShip(boolean hasShip) {
        this.hasShip = hasShip;
    }
}
