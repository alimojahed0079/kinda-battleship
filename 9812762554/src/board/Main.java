package board;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import temporary_classes.AlertBox;
import temporary_classes.ConfirmBox;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Main extends Application {

    /**
     * The main class is used for setting up environment and change scenes
     * */


    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader firstPageLoader = new FXMLLoader(getClass().getResource("FirstPage.fxml"));
        Parent firstPageRoot = firstPageLoader.load();
        FirstPage firstPageController = firstPageLoader.getController();
        Scene firstPageScene = new Scene(firstPageRoot);

        FXMLLoader sizeAndNamesLoader = new FXMLLoader(getClass().getResource("SizeAndNames.fxml"));
        Parent sizeAndNamesRoot = sizeAndNamesLoader.load();
        SizeAndNames sizeAndNamesController = sizeAndNamesLoader.getController();
        Scene sizeAndNamesScene = new Scene(sizeAndNamesRoot);

        FXMLLoader initialPlacingLoader = new FXMLLoader(getClass().getResource("InitialPlacingPlayer.fxml"));

        AtomicReference<InitialPlacingPlayer> initialPlacingController = new AtomicReference<>(initialPlacingLoader.getController());


        FXMLLoader gameBoardLoader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));

        GameBoard gameBoardController = gameBoardLoader.getController();

        firstPageController.getPlayGameButton().setOnAction(event -> {
            primaryStage.setScene(sizeAndNamesScene);
        });

        firstPageController.getQuit().setOnAction(event -> {
            event.consume();
            boolean answer = ConfirmBox.display("Quit", "Do you want to Exit");
            if (answer)
                primaryStage.close();

        });




        sizeAndNamesController.getInitialing().setOnAction(event -> {
            PlayerBoard.setNumberOfCells((int)sizeAndNamesController.getNumberOfCellsSlider().getValue());
            InitialPlacingPlayer.setPlayer1Name(sizeAndNamesController.getPlayer1Name().getText());
            InitialPlacingPlayer.setPlayer2Name(sizeAndNamesController.getPlayer2Name().getText());
            Parent initialPlacingRoot = null;
            try {
                initialPlacingRoot = initialPlacingLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert initialPlacingRoot != null;
            Scene initialPlacingScene = new Scene(initialPlacingRoot);
            primaryStage.setScene(initialPlacingScene);
            AtomicInteger temp = new AtomicInteger(1);
            initialPlacingController.set(initialPlacingLoader.getController());
            initialPlacingController.get().reset();

            initialPlacingController.get().getSetButton().setOnAction(e -> {
                if (temp.intValue() == 1) {

                    initialPlacingController.get().getSetButton().setText("Start Game");
                    GameBoard.setPlayer1(initialPlacingController.get().getPlayerBoards()[0]);
                    InitialPlacingPlayer.currentPlayer = 1;
                    initialPlacingController.get().reset();

                }
                if (temp.intValue() == 2)
                {
                    for (int i=0; i<11; i++)
                        initialPlacingController.get().getButtonContainer().getChildren().removeAll(initialPlacingController.get().getPlayerBoards()[0]);
                    initialPlacingController.get().getButtonContainer().getChildren().removeAll(initialPlacingController.get().getPlayerBoards()[1]);
                    GameBoard.setPlayer2(initialPlacingController.get().getPlayerBoards()[1]);
                    GameBoard.primaryStage = primaryStage;
                    Parent gameBoardRoot = null;
                    try {

                        gameBoardRoot = gameBoardLoader.load();
                    }
                    catch (IOException exception) {
                        exception.printStackTrace();
                    }
                        assert gameBoardRoot != null;
                        Scene gameBoardScene = new Scene(gameBoardRoot);
                        AlertBox.display("Colors", "Hey in case you want to know:\n" +
                                "Red means that a troop has destroyed, Yellow means that a trooper got hit\n" +
                                "Pink means your last not successful attack, Blue means troops places\n" +
                                "Orange means places you can attack and green means cells you can place your trooper!");
                        primaryStage.setScene(gameBoardScene);
//                        gameBoardController.getQuit().setOnAction(event1 -> {
//                        primaryStage.close();
//                        event1.consume();
//                        });
                    }
                temp.getAndIncrement();
            });
        });
        sizeAndNamesController.getBack().setOnAction(event -> {
            primaryStage.setScene(firstPageScene);
        });


        primaryStage.setScene(firstPageScene);
        primaryStage.setTitle("Kinda Battleship - Ali Mojahed");
        primaryStage.getIcons().add(new Image("file:src\\miscellaneous.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
