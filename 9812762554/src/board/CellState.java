package board;

/**
 * This enum is used for defining state of cells.
 * */


public enum CellState {
    HIT, EMPTY, DESTROY, HAS_TROOPER
}
