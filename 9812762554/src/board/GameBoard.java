package board;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import temporary_classes.AlertBox;
import troops.TroopsBluePrint;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class GameBoard implements Initializable {

    /**
     * GameBoard is the controller for GameBoard fxml file and actually  is where the magics happened.
     * */

    @FXML
    private AnchorPane player1GridContainer;
    @FXML
    private AnchorPane player2GridContainer;
    @FXML
    private GridPane buttonsContainer;
    @FXML
    private Label player1Info;
    @FXML
    private Label whoSTurn;
    @FXML
    private Label player2Info;
    @FXML
    private Button attackButton;
    @FXML
    private Button moveButton;
    @FXML
    private Button passButton;
    @FXML
    private GridPane buttonsContainerPl1;
    @FXML
    private GridPane buttonsContainerPl2;
    @FXML
    private AnchorPane quitButtonContainer;
    private PlayerBoard[] playerBoard;
    private static PlayerBoard player1;
    private static PlayerBoard player2;
    private int currentPlayer = 1;
    private Button quit;
    public static Stage primaryStage;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        playerBoard = new PlayerBoard[2];
        quit = new Button();
        quit.setText("please Click to end the game");
        quit.setOnAction(event -> {
            event.consume();
            primaryStage.close();
        });
        reset();
    }

    /**
     * The reset method is just set things up for gui show and set the information from last scene to this one and set up listeners
     * for the components we need to interact with.
     * */

    public void reset() {
        playerBoard[0] = player1;
        playerBoard[1] = player2;
        ArrayList<TroopsBluePrint> troopsPlayer1 = playerBoard[0].getTroops();
        ArrayList<TroopsBluePrint> troopsPlayer2 = playerBoard[1].getTroops();
        for (TroopsBluePrint trooper : troopsPlayer1) {
            trooper.setButton(new Button());
            trooper.getButton().setText(trooper.toString());
            trooper.getButton().setDisable(true);

            troopsListener(trooper);

        }
        for (TroopsBluePrint trooper : troopsPlayer2) {
            trooper.setButton(new Button());
            trooper.getButton().setText(trooper.toString());
            trooper.getButton().setDisable(true);

            troopsListener(trooper);
        }

        whoSTurn.setText(String.valueOf(currentPlayer));
        moveButton();
        attackButton();
        passButton();
        cellListener();
        setNewButtonContainer();
        changePlayer();
        playerBoard[0].repainting();
        playerBoard[1].repainting();
        player1GridContainer.getChildren().add(playerBoard[0]);
        player2GridContainer.getChildren().add(playerBoard[1]);


    }


    /**
     *
     * These methods all are listeners for components we need to interact with
     * */

    private void troopsListener(TroopsBluePrint trooper) {

        trooper.getButton().setOnAction(event -> {
            playerBoard[currentPlayer].setCurrentTrooper(trooper);
            if (playerBoard[currentPlayer].getState() == EventState.MOVE) {
                playerBoard[currentPlayer].makeGridReadyForMove();
            }


        });

    }

    private void cellListener() {
        Cell[][] grid1 = playerBoard[0].getGrid();
        Cell[][] grid2 = playerBoard[1].getGrid();
        int id = 0;
        for (int i = 0; i < PlayerBoard.getNumberOfCells(); i++) {
            for (int j = 0; j < PlayerBoard.getNumberOfCells(); j++) {
                try {
                    Cell temp = playerBoard[0].getCell(i, j);
                    mouseClickedHandler(temp);
                    mouseEnteredHandler(temp);
                    mouseExitedHandler(temp);

                    temp = playerBoard[1].getCell(i, j);
                    mouseClickedHandler(temp);
                    mouseEnteredHandler(temp);
                    mouseExitedHandler(temp);

                } catch (Exception e) {
                    System.out.println("not now" + id++);
                }
            }
        }
    }

    private void mouseEnteredHandler(Cell cell) {
        cell.setOnMouseEntered(event -> {
            if (cell.getBoard().getCurrentTrooper() != null) {
                if (cell.getBoard().getState() == EventState.MOVE) {
                    if (cell.isValidForPlacing()) {
                        cell.paintCells(Color.GREEN);
                    }
                }
            }
            if (playerBoard[currentPlayer].getCurrentTrooper() != null) {
                if (cell.getBoard().getState() == EventState.ATTACK) {
                    for (int i = 0; i < playerBoard[currentPlayer].getCurrentTrooper().attack()[0]; i++) {
                        for (int j = 0; j < playerBoard[currentPlayer].getCurrentTrooper().attack()[1]; j++) {
                            if (cell.isValidCoordinate(i, j)) {
                                if (cell.getBoard().getGrid()[cell.getX() + i][cell.getY() + j].isCanHit()) {
                                    cell.getBoard().getGrid()[cell.getX() + i][cell.getY() + j].getBorderRect().setFill(Color.ORANGE);
                                }
                            }
                        }
                    }
                }
            }

        });
    }

    private void mouseExitedHandler(Cell cell) {
        cell.setOnMouseExited(event -> {
            if (cell.getBoard().getCurrentTrooper() != null) {
                if (cell.getBoard().getState() == EventState.MOVE) {
                    if (cell.isValidForPlacing()) {
                        cell.getBoard().paintingForPlayer();
                    }
                }
            }
            if (cell.getBoard().getState() == EventState.ATTACK) {
                cell.getBoard().repainting();
            }

        });
    }

    private void mouseClickedHandler(Cell cell) {
        cell.setOnMouseClicked(event -> {
            if (playerBoard[currentPlayer] != null)
            {
                if (cell.getBoard().getState() == EventState.MOVE)
                {
                    if (cell.isValidForPlacing()) {
                        cell.paintCells(Color.BLUE);
                        cell.placeTrooper();
                        changePlayer();
                    }

                }
                else if (cell.getBoard().getState()== EventState.ATTACK)
                {
                    cell.getBoard().setLastXCoords(new ArrayList<Integer>());
                    cell.getBoard().setLastYCoords(new ArrayList<Integer>());
                    boolean condition = false;
                    for (int i = 0; i < playerBoard[currentPlayer].getCurrentTrooper().attack()[0]; i++) {
                        for (int j = 0; j < playerBoard[currentPlayer].getCurrentTrooper().attack()[1]; j++) {
                            if (cell.isValidCoordinate(i, j)) {

                                Cell temp = cell.getBoard().getGrid()[cell.getX() + i][cell.getY() + j];
                                if (temp.isCanHit()) {
                                    condition = true;
                                    if (temp.hasShip()) {

                                        temp.getTrooper().damage();
                                        if (temp.getTrooper().isDestroy()) {
                                            temp.getBorderRect().setFill(Color.RED);
                                            temp.setCellState(CellState.DESTROY);
                                        } else {
                                            temp.getBorderRect().setFill(Color.YELLOW);
                                            temp.setCellState(CellState.HIT);
                                        }
                                        temp.setCanHit(false);
                                    } else {
                                        cell.getBoard().getLastXCoords().add(temp.getX());
                                        cell.getBoard().getLastYCoords().add(temp.getY());
                                    }
                                }
                            }
                        }
                    }
                    playerBoard[currentPlayer].getCurrentTrooper().setWait(playerBoard[currentPlayer].getCurrentTrooper().getCoolDownDuration()+1);
                    playerBoard[currentPlayer].getCurrentTrooper().setReady(false);
                    cell.getBoard().repainting();

                    if (condition) {
                        changePlayer();
                    }
                }
                playerBoard[currentPlayer].setCurrentTrooper(null);
            }
        });
    }

    private void moveButton() {
        moveButton.setOnAction(event -> {
            playerBoard[currentPlayer].setState(EventState.MOVE);
            playerBoard[1 - currentPlayer].setState(EventState.NOTHING);
            buttonsShowForMoving();

            playerBoard[currentPlayer].paintingForPlayer();
            attackButton.setDisable(false);
            moveButton.setDisable(true);
            passButton.setDisable(false);
            AlertBox.display("A friendly reminder", "Make sure " + playerBoard[1-currentPlayer].getPlayerName()+ " doesn't watch your board 😉");
        });
    }

    private void attackButton() {
        attackButton.setOnAction(event -> {
            playerBoard[1 - currentPlayer].setState(EventState.ATTACK);
            playerBoard[currentPlayer].setState(EventState.NOTHING);
            buttonsShow();

            attackButton.setDisable(true);
            moveButton.setDisable(false);
            passButton.setDisable(false);
        });
    }

    private void passButton() {
        passButton.setOnAction(event -> {
            playerBoard[currentPlayer].setState(EventState.NOTHING);
            attackButton.setDisable(true);
            moveButton.setDisable(true);
            passButton.setDisable(true);
            changePlayer();
        });
    }

    /**
     * For the ease of changing players in order i just use an array of two elements and for changing
     * the player i just subtract current player index from one this way result is always one or zero
     * which can be our next index
     * */

    private void changePlayer() {
        if (endOfGame() || playerBoard[1-currentPlayer].getNumberOfCurrentTroops()==0)
        {
            System.out.println("End Of Game");
            endTheGame();
            endOfGame();
            return;
        }
        for (TroopsBluePrint trooper : playerBoard[currentPlayer].getTroops()) {
            trooper.decreaseWait();
        }

        playerBoard[currentPlayer].repainting();
        playerBoard[1-currentPlayer].repainting();
        playerBoard[currentPlayer].setState(EventState.NOTHING);

        this.currentPlayer = 1 - currentPlayer;
        playerBoard[currentPlayer].setState(EventState.NOTHING);
        player1Info.setText(player1.getPlayerName() + " : trooper: " + player1.getNumberOfCurrentTroops());
        player2Info.setText(player2.getPlayerName() + " : trooper: " + player2.getNumberOfCurrentTroops());
        whoSTurn.setText(playerBoard[currentPlayer].getPlayerName() + " Turns");
        for (TroopsBluePrint trooper : playerBoard[currentPlayer].getTroops()) {
            trooper.getButton().setDisable(true);
        }
        for (TroopsBluePrint trooper : playerBoard[1 - currentPlayer].getTroops()) {
            trooper.getButton().setDisable(true);
        }
        attackButton.setDisable(false);
        moveButton.setDisable(false);
        passButton.setDisable(false);


    }

    /**
     * This method is use to show buttons for the attack and disable troops which are destroyed or not ready at this round.
     * This method calls when attack button is pressed and event of the game is state of attack
     * */

    private void buttonsShow() {
        for (TroopsBluePrint trooper : playerBoard[currentPlayer].getTroops()) {
            if (trooper.isReady() && !trooper.isDestroy())
                trooper.getButton().setDisable(false);
        }
        for (TroopsBluePrint trooper : playerBoard[1 - currentPlayer].getTroops()) {
            trooper.getButton().setDisable(true);
        }
    }

    /**
     * This method is used to show buttons for movable troops.
     * */

    private void buttonsShowForMoving()
    {
        for (TroopsBluePrint trooper : playerBoard[currentPlayer].getTroops()) {
            if (!trooper.isHit())
                trooper.getButton().setDisable(false);
        }
        for (TroopsBluePrint trooper : playerBoard[1 - currentPlayer].getTroops()) {
            trooper.getButton().setDisable(true);
        }
    }
    private boolean hasTower()
    {

        for (TroopsBluePrint trooper: playerBoard[currentPlayer].getTroops())
        {
            if(trooper.toString().equals("Tower1") || trooper.toString().equals("Tower2"))
            {

                if (!trooper.isDestroy())
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * This method is used to place the buttons to the grid they belong to.
     * */
    private void setNewButtonContainer() {
        ArrayList<TroopsBluePrint> troops1 = playerBoard[0].getTroops();
        for (TroopsBluePrint trooper : troops1) {
            //trooper.getButton().setDisable(true);
        }
        buttonsContainerPl1.add(troops1.get(0).getButton(), 0, 0);
        buttonsContainerPl1.add(troops1.get(1).getButton(), 1, 0);
        buttonsContainerPl1.add(troops1.get(2).getButton(), 2, 0);
        buttonsContainerPl1.add(troops1.get(3).getButton(), 3, 0);
        buttonsContainerPl1.add(troops1.get(4).getButton(), 0, 1);
        buttonsContainerPl1.add(troops1.get(5).getButton(), 1, 1);
        buttonsContainerPl1.add(troops1.get(6).getButton(), 2, 1);
        buttonsContainerPl1.add(troops1.get(7).getButton(), 3, 1);
        buttonsContainerPl1.add(troops1.get(8).getButton(), 0, 2);
        buttonsContainerPl1.add(troops1.get(9).getButton(), 1, 2);
        buttonsContainerPl1.add(troops1.get(10).getButton(), 2, 2);


        ArrayList<TroopsBluePrint> troops2 = playerBoard[1].getTroops();
        buttonsContainerPl2.add(troops2.get(0).getButton(), 0, 0);
        buttonsContainerPl2.add(troops2.get(1).getButton(), 1, 0);
        buttonsContainerPl2.add(troops2.get(2).getButton(), 2, 0);
        buttonsContainerPl2.add(troops2.get(3).getButton(), 3, 0);
        buttonsContainerPl2.add(troops2.get(4).getButton(), 0, 1);
        buttonsContainerPl2.add(troops2.get(5).getButton(), 1, 1);
        buttonsContainerPl2.add(troops2.get(6).getButton(), 2, 1);
        buttonsContainerPl2.add(troops2.get(7).getButton(), 3, 1);
        buttonsContainerPl2.add(troops2.get(8).getButton(), 0, 2);
        buttonsContainerPl2.add(troops2.get(9).getButton(), 1, 2);
        buttonsContainerPl2.add(troops2.get(10).getButton(), 2, 2);


    }



    /**
     * This method determine whether game is finished or not.
     * */
    private boolean endOfGame() {
        return playerBoard[1 - currentPlayer].isAllDestroy();
    }


    /**
     * this methods run to stop the game flow and show the quit button to end the process.
     * */
    private void endTheGame()
    {
        AlertBox.display("Winner!!!", " Winner winner chicken dinner!\n" +
                playerBoard[currentPlayer].getPlayerName()+" Won");
        quitButtonContainer.getChildren().add(quit);
        whoSTurn.setText(playerBoard[currentPlayer].getPlayerName() + " is winner!!!!!!😎😎");
        moveButton.setDisable(true);
        attackButton.setDisable(true);
        passButton.setDisable(true);
        for (TroopsBluePrint trooper: playerBoard[currentPlayer].getTroops())
        {
            trooper.getButton().setDisable(true);
        }
        for (TroopsBluePrint trooper: playerBoard[1-currentPlayer].getTroops())
        {
            trooper.getButton().setDisable(true);
        }
    }

    /**
     *
     * With these two methods we set PlayerBoard with the boards we initialized at the last scene
     * */

    public static void setPlayer1(PlayerBoard player1) {
        GameBoard.player1 = player1;
    }

    public static void setPlayer2(PlayerBoard player2) {
        GameBoard.player2 = player2;
    }

    public Button getQuit() {
        return quit;
    }
}
