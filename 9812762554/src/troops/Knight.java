package troops;

public class Knight extends TroopsBluePrint{
    public Knight(int number) {
        super(2, 2, 1, 1, number);
        getButton().setText(this.toString());
    }

    @Override
    public int[] attack() {
        return new int[]{2, 1};
    }

    @Override
    public String toString() {
        return "Knight " + getNumber();
    }
}
